#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018-2019 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If NOT, see <http://www.gnu.org/licenses/>.

from base import sql

def check_points(user_id):
    with sql.Sql() as conn:
        cur = conn.cursor()
        cur.execute(
            "SELECT points from Attributes where id=?", [user_id])
        results = cur.fetchall()[0]
    if results <= 0:
        return False
    else:
        return True
