#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018-2019 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import pyowm
import pylast
import discord
from base import output
from base import sql
from base import exceptions
import datetime as dt
from collections import OrderedDict
from bs4 import BeautifulSoup
import requests
import random

def weather(parsed, conf):
    '''Outputs weather for user, other user, or chosen city.'''
    owm = pyowm.OWM(conf['weather']['api'])

    units = {
        'm' :   ['celsius', 'meters_sec'],
        'i' :   ['fahrenheit', 'miles_hour'],
        'k' :   ['kelvin', 'meters_sec']
    }

    # Gets saved user data or parses query
    if type(parsed['target']) != list:
        with sql.Sql() as conn:
            cur = conn.cursor()
            cur.execute('''
                SELECT unit, location
                FROM Attributes
                WHERE id=?''', [parsed['target'].id])
            results = cur.fetchall()
        if not results:
            raise exceptions.AttributeError('Please save user attributes.')
        unit = units[results[0][0]]
        location = results[0][1].split(' ')
        if not unit:
            unit = units[parsed['-u']]
        unit2 = units[parsed['-u2']]

    else:
        unit = units[parsed['-u']]
        unit2 = units[parsed['-u2']]
        location = parsed['target']

    # Prepares location
    country = location[-1]
    try:
        location[0] = location[0].strip(',')
        int(location[0])
        obs = owm.weather_at_zip_code(location[0], location[1])
    except ValueError:
        if location[-2][-1] != ',':
            location[-2] = location[-2] + ','
        obs = owm.weather_at_place(' '.join(location))
    city = obs.get_location().get_name()

    # Pulls weather information
    w = obs.get_weather()
    code = w.get_weather_code()
    status1 = w.get_status()
    status2 = w.get_detailed_status()

    if code in range(200, 599):
        prep = w.get_rain()
        try:
            prep = prep['3h']
            prep_description = ' mm of rain'
        except KeyError:
            prep = 'Minimal'
            prep_description = ' '
    elif code in range(600, 699):
        prep = w.get_snow()
        try:
            prep = prep['3h']
            prep_description = ' mm of snow'
        except KeyError:
            prep = 'Minimal'
            prep_description = ' '
    else:
        prep = 'None'
        prep_description = ' '

    time = w.get_reference_time(timeformat='iso')[:-6]
    clouds = str(w.get_clouds()) + '%'
    wind = (
        f'{w.get_wind(unit=unit[1])["speed"]} {unit[1][0]}/'
        f'{unit[1][unit[1].find("_")+1]}')
    humidity = str(w.get_humidity()) + '%'
    temp = str(w.get_temperature(unit=unit[0])['temp']) + unit[0][0].upper()
    temp2 = str(w.get_temperature(unit=unit2[0])['temp']) + unit2[0][0].upper()
    icon = w.get_weather_icon_url()

    data = {
        'city'      :   city,
        'country'   :   country,
        'status'    :   status1,
        'details'   :   status2,
        'prep'      :   prep,
        'pre_desc'  :   prep_description,
        'time'      :   time,
        'clouds'    :   clouds,
        'wind'      :   wind,
        'humidity'  :   humidity,
        'temp'      :   temp,
        'temp2'     :   temp2,
        'icon'      :   icon
    }
    out = output.Output(parsed, data).weather()

    return out

def set(parsed):
    # Removes non-set attributes and prepares which attributes to set
    to_set = {}
    cols = {
        '-u'    :   'unit',
        '-l'    :   'location',
        '-f'    :   'lastfm',
        '-s'    :   'sign'
    }
    for key, value in parsed.items():
        if key.startswith('-') and value is not None:
            to_set[cols[key]] = value

    # Gets number of attributes to set to prepare SQL statement
    cols = '(name, ' + ', '.join(to_set.keys()) + ', id)'
    col_names = ['name']
    col_names.extend(to_set.keys())
    col_names.append('id')
    prep_format = [x*2 for x in col_names]
    form = []
    for sub in prep_format:
        l = int(len(sub)/2)
        form.append(sub[:l])
        form.append(sub[l:])

    sets = (('{}=excluded.{}, ' * (len(to_set)+1)).format(*form))[:-2]
    vals = '(?, ' + ('?, ' * len(to_set)) + '?)'

    params = [parsed['user'].name]
    params.extend(to_set.values())
    params.append(parsed['user'].id)

    with sql.Sql() as conn:
        cur = conn.cursor()
        cur.execute(f'''
            INSERT INTO Attributes {cols}
            VALUES {vals}
            ON CONFLICT (id)
            DO UPDATE SET
            {sets}
            ''', params)
        conn.commit()

    data = dict(zip(cols[1:-1], vals[1:-1]))
    out = output.Output(parsed, data).set()

    return out

def fm(parsed, conf):
    '''Outputs lastfm stats'''
    network = pylast.LastFMNetwork(
        api_key=conf['lastfm']['key'],
        api_secret=conf['lastfm']['secret'],
        username=conf['lastfm']['user']
    )

    flags = OrderedDict({
        '-vvv'  :   'overall',
        '-vv'   :   '1month',
        '-v'    :   '7day'
    })
    periods = {
        'overall'   :   'Overall',
        '1month'    :   'Last Month',
        '7day'      :   'Last Week'
    }

    for key, value in parsed.items():
        if key.startswith('-v') and value:
            period = flags[key]
            break
    try:
        period
    except NameError:
        period = False

    # Gets saved user data or parses query
    if type(parsed['target']) != list:
        with sql.Sql() as conn:
            cur = conn.cursor()
            cur.execute('''
                SELECT lastfm
                FROM Attributes
                WHERE id=?''', [parsed['target'].id])
            results = cur.fetchall()
        if not results:
            raise exceptions.AttributeError('Please save user attributes.')
        fm = results[0][0]

    else:
        fm = parsed['target'][0]

    user = network.get_user(fm)
    url = user.get_url()
    np = user.get_now_playing()

    if np is None:
        recent = user.get_recent_tracks(limit=2)
        status = f'Previous Scrobble @ {recent[0].playback_date} GMT'
        np = recent[0].track
        track = np.title
        album = f' ({recent[0].album})'
        artist = np.artist.name
        art = network.get_album(artist, recent[0].album)
        # np.get_cover_image() - IndexError w/3.6.9 & pylast 3.1.0
    else:
        status = 'Currently Scrobbling'
        track = np.title
        album = ''  # np.get_album() returns None
                # network.get_album(np.artist, np.track) finds nada (WSError)
        artist = np.artist.name
        art = np.artist

    try:
        art_url = art.get_cover_image()
    except pylast.WSError:
        art_url = user.get_image()

    data = {
        'url'           :   url,
        'lastfm'        :   'LastFM: ' + user.name,
        'status'        :   status,
        'playing'       :   f'{artist} - {track}{album}',
        'track'         :   track,
        'artist'        :   artist,
        'album'         :   album,
        'icon'          :   art_url
    }

    if period:
        total = data['total'] = "{:,}".format(user.get_playcount())
        if parsed['-a']:
            data['recent'] = user.get_top_albums(period=period, limit=12)
        elif parsed['-t']:
            data['recent'] = user.get_top_tracks(period=period, limit=12)
        else:
            data['recent'] = user.get_top_artists(period=period, limit=12)

        recent_list = []
        for i in data['recent']:
            if parsed['-a'] or parsed['-t']:
                recent_list.append((i.item.title, str(i.weight)))
            else:
                recent_list.append((i.item.name, i.weight))

        recents = []
        for row in recent_list:
            recents.append(" - ".join(row))
        data['recent'] = '\n'.join(recents)
        data['period'] = periods[period]

    out = output.Output(parsed, data).fm()
    return out

def hs(parsed, conf):
    # Gets saved user data or parses query
    flags = {
        'aries'         :   1,  'taurus'        :   2,  'gemini'        :   3,
        'cancer'        :   4,  'leo'           :   5,  'virgo'         :   6,
        'libra'         :   7,  'scorpio'       :   8,  'sagittarius'   :   9,
        'capricorn'     :   10, 'aquarius'      :   11, 'pisces'        :   12
    }

    if type(parsed['target']) != list:
        with sql.Sql() as conn:
            cur = conn.cursor()
            cur.execute('''
                SELECT sign
                FROM Attributes
                WHERE id=?''', [parsed['target'].id])
            results = cur.fetchall()
        if not results:
            raise exceptions.AttributeError('Please save user attributes.')
        sign = results[0][0].lower()
        key = flags[sign]

    else:
        sign = parsed['target'][0].lower()
        key = flags[sign]

    page = requests.get(
        'https://www.horoscope.com/us/horoscopes/general/'
        f'horoscope-general-daily-today.aspx?sign={key}')
    soup = BeautifulSoup(page.text, 'html.parser')
    main_class = soup.find(class_='main-horoscope')
    for p in main_class.find('p'):
        content = main_class.find('p')
    # past: find(class_'date')
    date = content.find('strong').extract().get_text()
    content = content.get_text()
    text = content[3:]

    data = {
        'date'  :   date,
        'text'  :   text,
        'sign'  :   'Sign: ' + sign[0].upper() + sign[1:]
    }

    out = output.Output(parsed, data).hs()
    return out

def save(parsed, conf):
    date = dt.date.today()
    if parsed['user'].id in conf['wheel']:
        status = 0
        value = f"{parsed['save']} has been saved."
    else:
        status = 1
        value = f"{parsed['save']} must be approved first."
    vals = [
        parsed['save'], parsed['item'], parsed['user'].id, parsed['user'].name,
        date, status]

    with sql.Sql() as conn:
        cur = conn.cursor()
        cur.execute(f'''
            INSERT INTO Saves
            VALUES (?, ?, ?, ?, ?, ?)
            ''', vals)
        conn.commit()

    data = {
        'save'  :   parsed['save'],
        'item'  :   parsed['item'],
        'value' :   value
    }

    out = output.Output(parsed, data).save()
    return out

def get(parsed, conf):
    with sql.Sql() as conn:
        cur = conn.cursor()
        cur.execute(f'''
            SELECT save, item, name, date
            FROM Saves
            WHERE status=0 AND save=?''', [parsed['save']])
        results = cur.fetchall()
    if not results:
        raise exceptions.NotFoundError('Query does not exist')

    data = {
        'save'  :   results[0][0],
        'item'  :   results[0][1],
        'name'  :   results[0][2],
        'date'  :   results[0][3].strftime('%Y-%m-%d')
    }

    out = output.Output(parsed, data).get()
    return out

def mg(parsed, conf):
    if parsed['-l']:
        with sql.Sql() as conn:
            cur = conn.cursor()
            cur.execute(f'''
                SELECT date, name, save, item
                FROM Saves
                WHERE status=1''')
            results = cur.fetchall()
        if not results:
            raise exceptions.NotFoundError('Query does not exist')
        value = ''
    else:
        if parsed['-a']:
            results = parsed['-a'].split(' ')
            items = len(results) - 1
            where = ('WHERE save=? OR ' + ('save=? OR ' * items))[:-3]
            with sql.Sql() as conn:
                cur = conn.cursor()
                cur.execute(f'''
                    UPDATE Saves
                    SET status=0
                    {where}''', results)
                conn.commit()
            value = f"Save(s) {results} approved."
        elif parsed['-d']:
            results = parsed['-d'].split(' ')
            items = len(results) - 1
            where = ('WHERE save=? OR ' + ('save=? OR ' * items))[:-3]
            with sql.Sql() as conn:
                cur = conn.cursor()
                cur.execute(f'''
                    DELETE FROM Saves
                    {where}''', results)
                conn.commit()
            value = f"Save(s) {results} deleted."

    data = {
        'results'   :   results,
        'value'     :   value
    }

    out = output.Output(parsed, data).mg()
    return out

def gulag(parsed, conf):
    if parsed['-l']:
        with sql.Sql() as conn:
            cur = conn.cursor()
            cur.execute(f'''
                SELECT id, name
                FROM Attributes
                WHERE status=1''')
            results = cur.fetchall()
        if not results:
            raise exceptions.NotFoundError('Query does not exist')
        header = 'В гулаг'
    else:
        if parsed['-a']:
            status = 1
        elif parsed['-r']:
            status = 0

        status_dict = {1: 'в гулаг', 0: 'товарищи'}
        header = status_dict[status][0].upper()+status_dict[status][1:].lower()

        vals = ('(?, ?, ?),' * len(parsed['mentions']))[:-1]
        params = []
        for sub in parsed['mentions']:
            params.extend([sub.id, sub.name])
            params.append(status)
        results = [x for x in params[1::3]]

        with sql.Sql() as conn:
            cur = conn.cursor()
            cur.execute(f'''
                INSERT INTO Attributes (id, name, status)
                VALUES {vals}
                ON CONFLICT (id)
                DO UPDATE SET
                name=excluded.name, status=excluded.status
                ''', params)
            conn.commit()

    data = {
        'results'   :   results,
        'header'    :   header
    }

    out = output.Output(parsed, data).gulag()
    return out
def tay(parsed, conf):
    if parsed['-u']:
        with sql.Sql() as conn:
            cur = conn.cursor()
            cur.execute('''
                UPDATE Tay
                SET correction=?
                WHERE files LIKE ?
                ''', [parsed['era'], parsed['url']+'%'])
            conn.commit()
            cur.execute('''
                UPDATE TayP
                SET correction=?
                WHERE files LIKE ?
                ''', [parsed['era'], parsed['url']+'%'])
            conn.commit()
        data = {
            'header': 'Correction Recorded',
            'value': 'Thank you for helping improve SavoyBot!'
        }
        out = output.Output(parsed, data).tay()
    else:
        if parsed['era']:
            where = f"WHERE correction='{parsed['era']}'"
        else:
            where = ''

        with sql.Sql() as conn:
            cur = conn.cursor()
            cur.execute('''
                SELECT COUNT(FILES)
                FROM TayP''')

            total = cur.fetchall()[0][0]
            if total <= 1:
                cur.execute("DROP TABLE TayP")
                conn.commit()
                cur.execute("CREATE TABLE TayP AS SELECT * FROM Tay")
                conn.commit()

            cur.execute(f'''
                SELECT files, correction
                FROM TayP
                {where}''')
            results = cur.fetchall()

            if not results:
                cur.execute('''
                    INSERT INTO TayP
                    SELECT * FROM Tay
                    WHERE correction=?''', [parsed['era']])
                conn.commit()
                cur.execute(f'''
                    SELECT files, correction
                    FROM TayP
                    {where}''')
                results = cur.fetchall()

            row = random.choice(results)
            file = row[0]
            era = row[1].upper()
            cur.execute('''
                DELETE FROM TayP
                WHERE files=?''', [file])
            conn.commit()

            cur.execute('''
                SELECT COUNT(files)
                FROM TayP
                WHERE era=?''', [row[1]])
            count = cur.fetchall()[0][0]

        data = {
            'value' :   file,
            'header':   f"🚨🚨🚨 {era} ERA 🚨🚨🚨",
            'count' :   "{:,}".format(count),
            'total' :   "{:,}".format(total)
        }

        out = output.Output(parsed, data).tay()

    return out

def avi(parsed, conf):
    # Gets avi
    data = {
        'avi'  :   parsed['target'].avatar_url
    }

    out = output.Output(parsed, data).avi()
    return out

