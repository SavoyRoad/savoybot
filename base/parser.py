#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018-2019 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import discord
from base import man
from base import commands
from base import exceptions
from base import output
from base import sql
import re
from collections import defaultdict, OrderedDict
import pyowm
import pylast
import requests
import sqlite3

class MyOrderedDict():
    def __init__(self, ordered):
        self.odict = OrderedDict(ordered)
        self.okeys = list(self.odict.keys())
        self.last = len(self.okeys) - 1

    def next_key(self, key):
        nxt_loc = self.okeys.index(key) + 1

        if nxt_loc > self.last:
            nxt_loc = 0
        nxt = self.okeys[nxt_loc]

        return self.odict[nxt]

    def first_key(self):
        for key in self: return key
        raise ValueError("OrderedDict() is empty")

def flag_parser(parsed: dict, flags: dict, message: list):
    for flag in flags.keys():
        if flag in message:
            indx = message.index(flag) + 1
            parsed[flag] = None
            # List of correct choice (len 1)
            if type(flags[flag]) == list:
                msg = ' '.join(re.findall(rf'{flag} (\w+)', ' '.join(message)))
                if msg in flags[flag]:
                    parsed[flag] = msg
                else:
                    raise exceptions.ArgError(
                        'Please enter a correct argument value.')
            # String of item(s)
            elif type(flags[flag]) == int:
                arg_len = '{' + str(flags[flag] - 1) + '}'
                search = '{} (\S+ ){}(\S+)'.format(flag, arg_len)
                result = re.findall(search, ' '.join(message))
                if not result:
                    raise exceptions.ArgError(
                        'Please enter a correct argument value.')
                msg = ' '.join(re.findall(search, ' '.join(message))[0])
                parsed[flag] = msg.strip()
            # String of all items
            elif flags[flag] is True:
                search = '{} (.*?)(?:(-\w.*$|$))'.format(flag)
                result = re.findall(search, ' '.join(message))
                if not result:
                    raise exceptions.ArgError(
                        'Please enter a correct argument value.')
                msg = result[0][0]
                parsed[flag] = msg.strip()
            # Bool set to True
            elif flags[flag] is False:
                parsed[flag] = True
        elif re.findall(f'-\w*({flag[1:]})\w*', ' '.join(message)):
            parsed[flag] = True
        else:
            parsed[flag] = None

    for key, value in parsed.items():
        if key in message:
            try:
                message.remove(key)
            except ValueError:
                re.sub(f'(-\w*){flag[1:]}(\w*)', r'\1\2', ' '.join(message))
            if type(value) == list:
                for v in value:
                    if v in message:
                        message.remove(v)
            else:
                if value in message:
                    message.remove(value)
        elif re.findall(f'-\w*({key[1:]})\w*', ' '.join(message)):
            message = re.sub(
                f'(-\w*){key[1:]}(\w*)', r'\1\2', ' '.join(message))
            message = message.split(' ')

    parsed['message'] = message

    if parsed['message'] and parsed['target'] == parsed['user']:
        parsed['target'] = parsed['message']

    return parsed

class Parser():
    def __init__(self, ctx, message, conf):
        self.conf = conf
        self.user = ctx.message.author
        with sql.Sql() as conn:
            cur = conn.cursor()
            cur.execute(f'''
                SELECT status FROM Attributes
                WHERE id=?''', [self.user.id])
            results = cur.fetchall()
        try:
            if results[0][0] == 1 and self.user.id not in self.conf['wheel']:
                raise exceptions.GulagError
        except IndexError:
            pass

        # Discord-specific attributes
        self.ctx = ctx
        self.channel = ctx.message.channel
        self.server = ctx.message.guild
        self.mentions = ctx.message.mentions
        self.ch_mentions = ctx.message.channel_mentions
        self.attachments = ctx.message.attachments


        self.commands = dir(self)
        for meth in self.commands:
            if meth.startswith('__'):
                self.commands.remove(meth)

        self.message = list(message)
        self.flags = {
            '-x'    :   False,      # Non-embed output
            '-h'    :   False       # Help menu for command
        }

        # Quick check 
        if len(self.message) == 0:
            raise exceptions.ArgError('Please enter correct arguments.')

        # Parses through message, determining the command (if correct), the
        # general and command-specific flags, and passing the rest to the
        # command itself.
        self.command = self.message[0]
        if self.command not in self.commands:
            raise exceptions.CommandError('Please use a correct command.')
        else:
            del self.message[0]

        # Checks for general flags and removes from the message content
        #for flag in self.message:
            #if flag in self.flags.keys():
                #self.flags[flag] = True
                #self.message.remove(flag)
        for flag in self.flags.keys():
            if re.findall(f'-\w*({flag[1:]})\w*', ' '.join(self.message)):
                self.flags[flag] = True
                try:
                    self.message.remove(flag)
                except ValueError:
                    self.message = re.sub(f'(-\w*){flag[1:]}(\w*)', r'\1\2',
                           ' '.join(self.message))
                    self.message = self.message.split(' ')
        # Checks for mentions and removes from the message content
        for item in self.message:
            if re.match(r'<@\d{18}', item):
                self.message.remove(item)

        # Prep for parsed dict
        self.parsed = {}
        self.parsed['out'] = self.flags['-x']
        self.parsed['error'] = False
        self.parsed['message'] = []
        self.parsed['user'] = self.user

        if self.mentions:
            self.parsed['target'] = self.mentions[0]
        else:
            self.parsed['target'] = self.user

    def man(self):
        return man.help

    def sponge(self):
        if self.flags['-h']:
            return man.sponge

        message = ' '.join(self.message)
        out = [''] * len(message)
        out[::2], out[1::2] = message[::2].upper(), message[1::2].lower()
        out = ''.join(out)

        return out

    def clap(self):
        if self.flags['-h']:
            return man.clap

        message = ' '.join(self.message).upper()
        out = '👏 ' + message.replace(' ', ' 👏 ') + ' 👏'

        return out

    def weather(self):
        if self.flags['-h']:
            return man.weather

        cmd_flags = {
            '-u'    :   ['m', 'i', 'k']      # weather units
        }
        self.parsed = flag_parser(self.parsed, cmd_flags, self.message)

        # Validates queried arguments. Must contain a city/zip + country,
        # country code must be 2 letters.
        if self.parsed['message']:
            if len(self.parsed['target']) < 2:
                raise exceptions.ArgError()

        if self.parsed['-u'] == 'm':
            self.parsed['-u2'] = 'i'
        elif self.parsed['-u'] in ['i', 'k']:
            self.parsed['-u2'] = 'm'
        elif not self.parsed['-u']:
            self.parsed['-u'] = 'm'
            self.parsed['-u2'] = 'i'

        try:
            out = commands.weather(self.parsed, self.conf)
        except pyowm.exceptions.api_response_error.NotFoundError:
            self.parsed['error'] = True
            data = {
                'header':   'Something happened...',
                'value' :   'The selected location could not be found.'
            }
            out = output.Output(self.parsed, data).error()

        return out

    def fm(self):
        if self.flags['-h']:
            return man.fm

        cmd_flags = {
            '-vvv'  :   False,      # all time stats
            '-vv'   :   False,      # month stats
            '-v'    :   False,      # week stats
            '-a'    :   False,      # album mode of verbose
            '-t'    :   False       # track mode of verbose
        }
        self.parsed = flag_parser(self.parsed, cmd_flags, self.message)
        if self.parsed['-vvv'] and (self.parsed['-vv'] or self.parsed['-v']):
            try:
                self.parsed['-vv'] = False
                self.parsed['-v'] = False
            except KeyError:
                pass
        elif self.parsed['-vv'] and self.parsed['-v']:
            self.parsed['-v'] = False

        out = commands.fm(self.parsed, self.conf)

        return out

    def hs(self):
        if self.flags['-h']:
            return man.hs

        self.parsed = flag_parser(self.parsed, {}, self.message)

        try:
            out = commands.hs(self.parsed, self.conf)
        except KeyError:
            self.parsed['error'] = True
            data = {
                'header':   'Something happened...',
                'value' :   'The selected sign could not be found.'
            }
            out = output.Output(self.parsed, data).error()

        return out

    def set(self):
        if self.flags['-h']:
            return man.set

        cmd_flags = {
            '-l'    :   True,               # location arguments required
            '-u'    :   ['m', 'i', 'k'],    # weather unit choices
            '-f'    :   1,                  # lastfm username
            '-s'    :   1                   # horoscope sign
        }
        self.parsed = flag_parser(self.parsed, cmd_flags, self.message)

        out = commands.set(self.parsed)

        return out
    def save(self):
        if self.flags['-h']:
            return man.save

        self.parsed = flag_parser(self.parsed, {}, self.message)
        # Does not use parsed.target, relies solely on parsed.message
        if len(self.parsed['message']) < 2:
            raise exceptions.ArgError

        self.parsed['save'] = self.message[0]
        self.parsed['item'] = ' '.join(self.message[1:])
        try:
            out = commands.save(self.parsed, self.conf)
        except sqlite3.IntegrityError:
            self.parsed['error'] = True
            data = {
                'header':   'Something happened...',
                'value' :   'Record already exists.'
            }
            out = output.Output(self.parsed, data).error()

        return out

    def get(self):
        if self.flags['-h']:
            return man.get

        cmd_flags = {
            '-m'    :   False   # view details of the save
        }
        self.parsed = flag_parser(self.parsed, cmd_flags, self.message)
        # Does not use parsed.target, relies solely on parsed.message
        if len(self.parsed['message']) < 1:
            raise exceptions.ArgError

        self.parsed['save'] = self.message[0]
        try:
            out = commands.get(self.parsed, self.conf)
        except sqlite3.IntegrityError:
            self.parsed['error'] = True
            data = {
                'header':   'Something happened...',
                'value' :   'Record does not exist.'
            }
            out = output.Output(self.parsed, data).error()

        return out

    def g(self):
        out = self.get()
        return out
    def mg(self):
        if not self.parsed['user'].id in self.conf['wheel']:
            raise exceptions.PermissionError
        if self.flags['-h']:
            return man.mg

        cmd_flags = {
            '-l'    :   False,  # view list of pending saves
            '-a'    :   True,   # approve a save
            '-d'    :   True    # delete a save
        }
        self.parsed = flag_parser(self.parsed, cmd_flags, self.message)

        try:
            out = commands.mg(self.parsed, self.conf)
        except exceptions.ArgError:
            self.parsed['error'] = True
            data = {
                'header':   'Something happened...',
                'value' :   'No pending saves to approve.'
            }
            out = output.Output(self.parsed, data).error()
        except sqlite3.IntegrityError:
            self.parsed['error'] = True
            data = {
                'header':   'Something happened...',
                'value' :   'Record does not exist.'
            }
            out = output.Output(self.parsed, data).error()
        except exceptions.NotFoundError:
            self.parsed['error'] = True
            data = {
                'header':   'No pending saves.',
                'value' :   'Все хорошо!'
            }
            out = output.Output(self.parsed, data).error()

        return out

    def gulag(self):
        if not self.parsed['user'].id in self.conf['wheel']:
            raise exceptions.PermissionError
        if self.flags['-h']:
            return man.gulag

        cmd_flags = {
            '-l'    :   False,  # view list of gulagged users
            '-a'    :   False,  # add user to gulag
            '-r'    :   False   # remove user from gulag
        }
        self.parsed = flag_parser(self.parsed, cmd_flags, self.message)
        self.parsed['mentions'] = self.mentions
        if not self.parsed['-l'] and not self.mentions:
            self.parsed['error'] = True
            data = {
                'header':   'Something happened...',
                'value' :   'Select a user to send/release from the gulag.'
            }
            out = output.Output(self.parsed, data).error()
        if self.parsed['-a'] and self.parsed['-r']:
            self.parsed['error'] = True
            data = {
                'header':   'Something happened...',
                'value' :   'Too lazy, can only use one flag at a time.'
            }
            out = output.Output(self.parsed, data).error()

        out = commands.gulag(self.parsed, self.conf)

        return out
    def tay(self):
        if self.flags['-h']:
            return man.tay

        cmd_flags = {
            '-u'    :   2   # update a picture with an era correction
        }
        self.parsed = flag_parser(self.parsed, cmd_flags, self.message)

        if self.parsed['-u']:
            self.parsed['-u'] = self.parsed['-u'].split(' ')
            try:
                to_del = self.parsed['-u'].index('')
                del self.parsed['-u'][to_del]
            except ValueError:
                pass

            self.parsed['url'] = [x for x in self.parsed['-u'] if
                                  x.startswith('http')][0]
            self.parsed['url'] = re.findall(r'(^.*)(?:\/\w+\.\w+$)',
                                            self.parsed['url'])[0]
            self.parsed['era'] = [x for x in self.parsed['-u'] if not
                                  x.startswith('http')][0].lower()
        else:
            try:
                self.parsed['era'] = self.message[0].lower()
            except IndexError:
                self.parsed['era'] = False

        eras = {
            'debut'     :   'debut',
            'fetus'     :   'debut',
            'fearless'  :   'fearless',
            'fs'        :   'fearless',
            'speaknow'  :   'speaknow',
            'sn'        :   'speaknow',
            'red'       :   'red',
            '1989'      :   '1989',
            'reputation':   'reputation',
            'rep'       :   'reputation',
            'lover'     :   'lover'
        }
        if self.parsed['era'] and self.parsed['era'] in eras.keys():
            self.parsed['era'] = eras[self.parsed['era']]
        elif self.parsed['era'] and self.parsed['era'] not in eras.keys():
            self.parsed['error'] = True
            data = {
                'header':   'Something happened...',
                'value' :   f"{self.parsed['era']} era does not exist."
            }
            out = output.Output(self.parsed, data).error()
            return out

        out = commands.tay(self.parsed, self.conf)

        return out
    def avi(self):
        if self.flags['-h']:
            return man.avi

        self.parsed = flag_parser(self.parsed, {}, self.message)

        out = commands.avi(self.parsed, self.conf)

        return out

    def send(self):
        if not self.parsed['user'].id in self.conf['wheel']:
            raise exceptions.PermissionError
        if self.flags['-h']:
            return man.send

        cmd_flags = {
            '-user'    :   True,  # targets to send to, 'mailer' to send to list
            '-message' :   True,  # message to send
        }
        self.parsed = flag_parser(self.parsed, cmd_flags, self.message)

        if not self.parsed['-message'] or not self.parsed['-user']:
            self.parsed['error'] = True
            data = {
                'header':   'Something happened...',
                'value' :   'Select a user(s) and a message to send.'
            }
            out = output.Output(self.parsed, data).error()
        else:
            self.parsed['-user'] = self.parsed['-user'].split(' ')
            # Removes mentions from list as they're parsed differently in __main__
            for key, u in enumerate(self.parsed['-user']):
                if u.startswith('<'):
                    del self.parsed['-user'][key]
            data = {
                'header':   'Messages sent!',
                'value' :   '😏'
            }
            out = output.Output(self.parsed, data).send()

        return out

    def purge(self):
        if not self.parsed['user'].id in self.conf['wheel']:
            raise exceptions.PermissionError
        if self.flags['-h']:
            return man.purge

        self.parsed = flag_parser(self.parsed, {}, self.message)

        data = {
            'header':   'PURGED',
            'value' :   "Where's that icepick when you need one..."
        }
        out = output.Output(self.parsed, data).purge()

        return out
